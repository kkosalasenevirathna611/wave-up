WaveUp es una app que <i>despierta tu móvil</i>, es decir, enciende la pantalla, cuando <i>pasas la mano</i> por encima del sensor de proximidad.

Me puse a desarrollar esta app porque quería evitar darla al botón de encendido para ver qué hora es, cosa que parezco hacer con bastante frecuencia y alegría. De hecho, ya existen otras apps que hace esto e incluso más. Un buen ejemplo de las mismas, que de hecho inspiró la creación de WaveUp, es Gravity Screen On/Off, una app <b>muy buena</b>. Aun así, siendo un gran fan del software de código libre y no habiendo una app libre que realizara esta función, decidí ponerme yo mismo a ello. Si tienes interés, puedes encontrar el código de WaveUp aquí: 
https://gitlab.com/juanitobananas/wave-up

Simplemente pasa la mano por encima del sensor de proximidad para encender la pantalla de tu teléfono. A este modo se le llama <i>modo "wave"</i> y se puede desactivar para evitar encendidos accidentales de la pantalla si así lo deseas.

También se encenderá cuando saques el móvil del bolso o bolsillo. A este modo se le llama, de manera original,<i>modo bolsillo</i>. También éste se puede desactivar.

Ambos modos están activados por defecto.

WaveUp también es capaz de apagar la pantalla si tapas el sensor de proximidad durante un tiempo predeterminado. Aunque este modo no tiene un nombre específico, también se puede configurar en la pantalla principal de la app. Este modo no está activado por defecto.

Para aquéllos que no saben lo que es el sensor de proximidad: es un dispositivo pequeño situado en la parte superior del móvil (cerca de donde pones la oreja a la hora de realizar una llamada). Prácticamente no se puede ver. Es el responsable de apagar la pantalla cuando estás realizando una llamada.

<b>Desinstalación</b>

Esta app utiliza el "permiso de administrador del dispositivo". Por esto, no se puede desintalar WaveUp como lo harías con cualquier otra app.

Para desinstalarla, basta con que le des al botón "Desinstalar WaveUp" que se encuentra abajo del todo en la app misma.

<b>Problemas conocidos</b>

Desafortunadamente, algunos móviles mantienen la CPU encendida mientras están "escuchando" al sensor de proximidad. A esto se le llama <i>wake lock</i> y produce una descarga muy rápida de la batería. Esto no es mi culpa y no puedo hacer nada para evitarlo. La gran mayoría de los móviles se "irán a dormir" mientras la pantalla esté apagada mientras continúan leyendo los valores del sensor de proximidad. En este caso, la descarga de batería es despreciable.

<b>Permisos requeridos:</b>

▸ WAKE_LOCK para encender la pantalla
▸ USES_POLICY_FORCE_LOCK para bloquear el móvil
▸ RECEIVE_BOOT_COMPLETED para iniciar WaveUp cuando enciendes (o reinicias) el móvil
▸ READ_PHONE_STATE para apagar WaveUp mientras estás realizando una llamada

<b>Miscelánea</b>

Esta es la primera app para Android que escribo, o sea que, ¡ojo!

También es mi primera contribución al mundo del software libre. ¡Por fin!

Me encantaría oír tu opinión o que colaboraras de algún modo. Si es que te apetece, claro.

¡Gracias por leer esto!

¡¡¡El software de código libre es la caña!!!

<b>Traducciones</b>

Estaría muy bien si pudieras ayudar a traducir WaveUp a tu propio idioma (o incluso mejorar el texto en inglés). El proyecto se puede traducir utilizando transifex: https://www.transifex.com/juanitobananas/waveup/

<b>Agradecimientos</b>

Agradezco especialmente a las siguientes personas:

https://gitlab.com/juanitobananas/wave-up/#acknowledgments