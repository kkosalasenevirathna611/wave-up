New in 3.0.6
★ Add menu item to send debug logs to dev
★ If for some reason the device admin or accessibility service are disabled, show a dialog while reopening the app.
★ (Only paid apps) Show a dialog asking the user *not* to uninstall the app immediately (unless s/he wants a refund).

New in 3.0.5
★ Fix small bug in settings logic.
★ Add Hebrew and (incomplete) Turkish translations.
★ Update Danish and Japanese translations.
