New in 3.2.9
★ Update some translations.
★ Fix small Android 11 compatibility bugs.

New in 3.2.8
★ Update some translations.
★ Small bug fix (MainActivity leak).

New in 3.2.7
★ Fix compatibility with older Androids.
★ Update some translations.
★ Vibration time before locking is now configurable.
